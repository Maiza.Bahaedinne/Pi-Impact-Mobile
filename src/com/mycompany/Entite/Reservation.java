/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entite;

import java.util.Date;

/**
 *
 * @author koussai
 */
public class Reservation {
    private int id  ;  
    private Date DateRes ;
    private int  iduser ;  
    private int idevent;  

    public Reservation(int iduser, int idevent) {
        this.iduser = iduser;
        this.idevent = idevent;
    }

    public Reservation() {
    }

    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateRes() {
        return DateRes;
    }

    public void setDateRes(Date DateRes) {
        this.DateRes = DateRes;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public int getIdevent() {
        return idevent;
    }

    public void setIdevent(int idevent) {
        this.idevent = idevent;
    }

    @Override
    public String toString() {
        return "Reservation{" + "id=" + id + ", DateRes=" + DateRes + ", iduser=" + iduser + ", idevent=" + idevent + '}';
    }
   
    
}
