package com.mycompany.gui;


import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.TextModeLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.Evenement;
import com.mycompany.Entite.Reservation;
import com.mycompany.Formulaire.MyApplication;
import com.mycompany.Service.EvenementService;
import com.mycompany.Service.ReservationService;
import java.io.IOException;
import java.util.ArrayList;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author koussai
 */
public class EvenementGUI {
    
    private Form current,Acceuil,home; 
    Form f;
    SpanLabel lb;
    String url="";
    ImageViewer imageViewer;
    EncodedImage imgEncodedImg;
    Image urlImage;
    Resources theme1 =UIManager.initFirstTheme("/theme");
    EvenementService serviceTask=new EvenementService();
    ReservationService ResTas=new ReservationService();
    ArrayList<Reservation> ListeRes = ResTas.getListReservation() ; 
    
    
    public  Reservation  valideP (int iduser ,int idevent ) {
        
        for (Reservation R : ListeRes )
        {
        if ( (R.getIdevent() == idevent ) && (R.getIduser()== iduser) )
        {
        return R ;
        }
        }
        
     return  null  ;
    }
    
    
    public EvenementGUI() throws IOException {
        TextModeLayout tm = new TextModeLayout(4, 2);
        f = new Form("Evenement");
        
        ArrayList<Evenement> lis=serviceTask.getListeEvenement();
        Container c = new Container(BoxLayout.y());
        c.setScrollableY(true);
        for (Evenement E : lis){
        Container c1 = new Container(BoxLayout.y());
        Container DetailsEvent = new Container(BoxLayout.y());
        Container Date_Prix = new Container(BoxLayout.y());
        Container Buttons = new Container(BoxLayout.x());
        DetailsEvent.add(new Label(E.getTITRE_E()));
        SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
        String dateD = formater.format(E.getDATEDEBUT_E());
        String dateF = formater.format(E.getDATEFIN_E()); 
        Date_Prix.add(new SpanLabel("Debut : "+String.valueOf(dateD))) ;
        DetailsEvent.add(new SpanLabel("Frais : "+String.valueOf(E.getFRAIS_E()) + " DT")) ;
        
        Button P = new Button() ;
       
        if  (valideP(MyApplication.currentUser.getId(), E.getID_EVENEMENT())!=null)
        {
            P.setText("Annuler");

            P.addActionListener((evt) -> {
                 Reservation  R = valideP(MyApplication.currentUser.getId(), E.getID_EVENEMENT());
                serviceTask.SuppParticipatipation( R.getId()) ; 
            P.setText("j-y vais");
             try {
                    ListeRes = ResTas.getListReservation() ;
                } catch (IOException ex) {
             
                }
            });
           
        } 
        else
        {
            P.setText("j-y vais");
            
            P.addActionListener((evt) -> {
                serviceTask.AjoutParticipatipation(new Reservation(MyApplication.currentUser.getId(), E.getID_EVENEMENT())) ; 
                P.setText("Annuler");
                try {
                ListeRes = ResTas.getListReservation() ;
                } catch (IOException ex) {
             
                }
            });
            
           
        }
                
     
        
        
        
        Button PD = new Button("Plus d'information") ;
        
        
        PD.addActionListener((evt) -> {
                EvenementDetailsGUI EDGUI = new EvenementDetailsGUI(E) ;
                EDGUI.getF().show();
            
        });
        Buttons.add(P) ;
        Buttons.add(PD) ;
        
        url="http://localhost/image/"+E.getAFFICHE_E() ;
            System.out.println("path = "+url);
            imgEncodedImg  = EncodedImage.createFromImage(theme1.getImage("round.png"),false);
            urlImage = URLImage.createToStorage(imgEncodedImg, url, url).scaled(300 , 300);
            imageViewer = new ImageViewer(urlImage);
        
        
        c1.add(imageViewer) ; 
        c1.add(DetailsEvent) ; 
        DetailsEvent.add(Date_Prix) ;
        DetailsEvent.add(Buttons) ;
        
        c.add(c1) ;
        }
        
        f.add(c) ;
          
        
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
}
