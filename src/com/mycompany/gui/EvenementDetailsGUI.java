/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui;


import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.googlemaps.MapContainer;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.maps.Coord;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.mycompany.Entite.Evenement ; 
import com.mycompany.Formulaire.MyApplication;
import java.io.IOException;

/**
 *
 * @author koussai
 */
public class EvenementDetailsGUI {
    
    private Form current, Acceuil, home;
    Form f;
    SpanLabel lb;
    String url = "";
    ImageViewer imageViewer;
    EncodedImage imgEncodedImg;
    Image urlImage;
    Resources theme1 = UIManager.initFirstTheme("/theme");

      
    public EvenementDetailsGUI(Evenement E ) {
        
        
        f = new Form(E.getTYPE_E() +": "+ E.getTITRE_E());
         
        
        Container DetailsEvent = new Container(BoxLayout.y());
                
            url="http://localhost/image/"+E.getAFFICHE_E() ;
            System.out.println("path = "+url);
            imgEncodedImg  = EncodedImage.createFromImage(theme1.getImage("round.png"),false);
            urlImage = URLImage.createToStorage(imgEncodedImg, url, url).scaled(300 , 300);
             
            imageViewer = new ImageViewer(urlImage);
            
            
            
        DetailsEvent.add(imageViewer) ;
        SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yyyy");
        String dateD = formater.format(E.getDATEDEBUT_E());
        String dateF = formater.format(E.getDATEFIN_E());
        
        DetailsEvent.add(new SpanLabel("Debut : "+String.valueOf(dateD))) ;
        DetailsEvent.add(new SpanLabel("Fin : "+String.valueOf(dateF))) ;
        
        DetailsEvent.add(new SpanLabel( "Descriptiion : \n"+ E.getDESCRIPTION_E())) ;
        DetailsEvent.add(new SpanLabel( "Frais :"+ E.getFRAIS_E())) ;
        DetailsEvent.add(new SpanLabel( "Contact :"+ E.getCONTACT_E())) ;
        
        Coord cor = new Coord(E.getLatitude(), E.getLongitude()) ;    
     
         MapContainer map = new MapContainer();
          
        map.setCameraPosition(cor);
        map.zoom(cor, 4);
        map.addMarker(new MapContainer.MarkerOptions(cor, imgEncodedImg)) ;
                
        DetailsEvent.add(map) ;
       
        
        f.setScrollableY(true);
        f.add(DetailsEvent) ; 
      
       f.getToolbar().addCommandToRightBar("back", null, (ev)->{
                try {
                    EvenementGUI EG = new EvenementGUI() ;
                    EG.getF().show();
                } catch (IOException ex) {
                    
                }
          });
      
    }
    
     public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    
}
