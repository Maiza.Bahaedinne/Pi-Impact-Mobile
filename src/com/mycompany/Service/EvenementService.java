/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.mycompany.Entite.Evenement ; 
import com.mycompany.Entite.Reservation;
import java.util.Date;

/**
 *
 * @author koussai
 */
public class EvenementService {
            ConnectionRequest con = new ConnectionRequest();

    
    public ArrayList<Evenement> getListeEvenement() {
        ArrayList<Evenement> listTasks = new ArrayList<>();
            con.setUrl("http://127.0.0.1/Pi-Impact-Web/web/app_dev.php/Evenement/AfficheEvenetMobile");
       
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
              
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    System.out.println(tasks);
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                    for (Map<String, Object> obj : list) {
                        Evenement task = new Evenement();
                        float id = Float.parseFloat(obj.get("id").toString());
                        task.setID_EVENEMENT((int) id);
                        task.setTITRE_E(obj.get("titre").toString());
                        task.setDESCRIPTION_E(obj.get("description").toString());

                        Map<String,Object> date = (Map<String,Object>) obj.get("dateDebut");
                        float d = Float.parseFloat(date.get("timestamp").toString());
                        Date dates = new Date((long) ( d - 3600 ) * 1000 );
                        task.setDATEDEBUT_E(dates) ; 

                        Map<String,Object> dateF = (Map<String,Object>) obj.get("dateDebut");
                        float dF = Float.parseFloat(date.get("timestamp").toString());
                        Date datesF = new Date((long) ( dF - 3600 ) * 1000 );
                        task.setDATEFIN_E(datesF) ; 
                        
                        task.setFRAIS_E(Float.parseFloat(obj.get("frais").toString()));
                        task.setLatitude(Double.parseDouble(obj.get("latitude").toString()));
                        task.setLatitude(Double.parseDouble(obj.get("latitude").toString()));
                        task.setAFFICHE_E(obj.get("affiche").toString());
                        task.setCONTACT_E(obj.get("contact").toString());
                        task.setTYPE_E(obj.get("typeE").toString());

                        
                        listTasks.add(task);

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        
        return listTasks;
    }
    
    public void AjoutParticipatipation (Reservation reservation){
        ConnectionRequest c = new ConnectionRequest();
        String url = "http://127.0.0.1/Pi-Impact-Web/web/app_dev.php/Reservation/newMobile?idEvent=" +
        reservation.getIdevent()+"&idUser="+reservation.getIduser();
        c.setUrl(url);
        c.addResponseListener((e)->{
            String s = new String(c.getResponseData());
        });
        NetworkManager.getInstance().addToQueueAndWait(c);
    }
    
    public void SuppParticipatipation (Integer id){
        ConnectionRequest c = new ConnectionRequest();
        String url = "http://127.0.0.1/Pi-Impact-Web/web/app_dev.php/Reservation/"+id+"/deleteMobile";
        c.setUrl(url);
        c.addResponseListener((e)->{
            String s = new String(c.getResponseData());
        });
        NetworkManager.getInstance().addToQueueAndWait(c);
    }

}
