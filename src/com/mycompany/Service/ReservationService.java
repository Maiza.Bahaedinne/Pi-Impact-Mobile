/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.mycompany.Entite.Evenement;
import com.mycompany.Entite.Reservation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author koussai
 */
public class ReservationService {
               ConnectionRequest con = new ConnectionRequest();

    
    public ArrayList<Reservation> getListReservation() throws IOException  {
        ArrayList<Reservation> listTasks = new ArrayList<>();
            con.setPost(false);
            con.fetchJSON("http://127.0.0.1/Pi-Impact-Web/web/app_dev.php/Reservation/ReservationMobile");
            con.setUrl("http://127.0.0.1/Pi-Impact-Web/web/app_dev.php/Reservation/ReservationMobile");
           
            con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
            
                //listTasks = getListTask(new String(con.getResponseData()));
                JSONParser jsonp = new JSONParser();
                
                try {
                    Map<String, Object> tasks = jsonp.parseJSON(new CharArrayReader(new String(con.getResponseData()).toCharArray()));
                    
                    //System.out.println(tasks);
                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
                    for (Map<String, Object> obj : list) {
                        Reservation task = new Reservation();
                        
                        float id = Float.parseFloat(obj.get("id").toString());
                        float id1 = Float.parseFloat(obj.get("idEvent").toString());
                        float id2 = Float.parseFloat(obj.get("iduser").toString());
                        
                        task.setId((int) id);
                        task.setIdevent((int) id1);
                        task.setIduser((int) id2);
                        
                        
                        listTasks.add(task);
                        

                    }
                } catch (IOException ex) {
                }

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        
        return listTasks;
    }
       
}
